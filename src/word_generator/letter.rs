extern crate rustc_serialize;

use std::fmt;
use rustc_serialize::json::{self,Json};
use rustc_serialize::{Decodable,Decoder};

#[derive(Clone,Debug)]
pub enum LetterType{
	Vowel,
	Consonant
}

#[derive(Clone,Debug)]
pub struct Letter{
	value : char,
	pub letter_type : LetterType,
	second_letters: Vec<char>,
	second_from_last_letters: Vec<char>
}

impl Decodable for Letter {
    fn decode<D: Decoder>(d: &mut D) -> Result<Letter, D::Error> {

    	let s_letter_type :String = try!(d.read_struct_field("letter_type", 0, |d| Decodable::decode(d)));

		let letter_type : LetterType = match &*s_letter_type {
			"vowel" => LetterType::Vowel,
			"consonant" => LetterType::Consonant,
			_ => panic!("AAH")
		};

	     Ok(Letter{
	      value: try!(d.read_struct_field("value", 0, |d| Decodable::decode(d))),
	      letter_type: letter_type,
	      second_letters: try!(d.read_struct_field("second_letters", 0, |d| Decodable::decode(d))),
	      second_from_last_letters: try!(d.read_struct_field("second_from_last_letters", 0, |d| Decodable::decode(d))),
	    })
    }
}

impl fmt::Display for Letter{

	fn fmt(&self, f: &mut fmt::Formatter)->fmt::Result{

		write!(f,"[{:?}: {:?}]",self.letter_type,self.value)

	}

}
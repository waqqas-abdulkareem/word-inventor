extern crate rustc_serialize;

use word_generator::letter::{Letter,LetterType};
use rustc_serialize::json::{self,Json};

#[derive(Debug)]
pub struct Alphabet{
	vowels: Vec<Letter>,
	consonants: Vec<Letter>
}

impl Alphabet{

	pub fn new()->Alphabet{

		return Alphabet{
			vowels: Vec::new(),
			consonants: Vec::new()
		}

	}

	fn add_letter(&mut self,letter: Letter){

		match letter.letter_type {
			LetterType::Vowel => self.vowels.push(letter),
			LetterType::Consonant => self.consonants.push(letter)

		}

	}

	pub fn vowels(&self)->Vec<Letter>{
		return self.vowels.clone()
	}

	pub fn from_json(json: &str)->Alphabet{

		let data :Json = match Json::from_str(json){
			Ok(_data)=>_data,
			Err(_err)=>panic!("Invalid JSON provided")
		};

		let letters = match data.as_array(){
			Some(_letters)=>_letters,
			None=>panic!("Expected JSON's root to be an array but found a different structure.")
		};

		let mut it = letters.iter();

		let mut alphabet = Alphabet::new();

		loop {
			match it.next(){
				Some(x) =>{
					
					let letter : Letter= json::decode(&(x.to_string())).unwrap();
					alphabet.add_letter(letter);

				},
				None => break,
			}
		}

		return alphabet
	}
}
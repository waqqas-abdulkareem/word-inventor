mod word_generator;
extern crate rustc_serialize;

use rustc_serialize::json::{self,Json};
use word_generator::letter::{Letter};
use word_generator::alphabet::{Alphabet};


fn main(){
	let json = "[ { \"value\":\"A\", \"letter_type\":\"vowel\", \"second_letters\":[\"*\"], \"second_from_last_letters\":[\"*\"] }, { \"value\":\"B\", \"letter_type\":\"consonant\", \"second_letters\":[\"L\",\"Y\"], \"second_from_last_letters\":[\"L\",\"Y\"] }, { \"value\":\"L\", \"letter_type\":\"consonant\", \"second_letters\":[\"Y\"], \"second_from_last_letters\":[\"B\"] }, { \"value\":\"Y\", \"letter_type\":\"consonant\", \"second_letters\":[], \"second_from_last_letters\":[\"B\",\"L\"] } ]";

	/*let data = Json::from_str(json).unwrap();

	let obj = data.as_object().unwrap();

	let letters = obj.get("letters").unwrap().as_array().unwrap();

	let mut it = letters.iter();
	loop {
		match it.next(){
			Some(x) =>{
				
				let letter : Letter= json::decode(&(x.to_string())).unwrap();
				println!("Letter: {}",letter)
			},
			None => break,
		}
	}*/
	let alphabet = Alphabet::from_json(json);
	println!("{:?}",alphabet.vowels());
}
